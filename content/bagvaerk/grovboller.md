---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Grovboller {.unnumbered}

![](../_assets/grovboller.jpg)

Du får 30 meget store eller 40 normale boller ud af portionen.

## Ingredienser

|                   | Oprindelig |   2/3 |   1/2 |
|-------------------|-----------:|------:|------:|
| Hvedemel          |     1200 g | 800 g | 600 g |
| Fuldkornshvedemel |      400 g | 267 g | 200 g |
| Rugmel            |      200 g | 133 g | 100 g |
| Ymer              |      400 g | 267 g | 200 g |
| Vand, 30° C varmt |      600 g | 400 g | 300 g |
| Gær               |      100 g |  67g  |  50 g |
| Mørk farin        |       30 g |  20 g |  15 g |
| Salt              |       30 g |  20 g |  15 g |
| Smør              |      100 g |  67 g |  50 g |

## Procedure

1. Dejen blandes.
2. Hæver i 20 minutter.
3. Derefter slås dejen sammen.
4. Hæver i 20 minutter mere.
5. Formes til boller eller brød.
6. Hæver i 1 time et lunt sted.
7. Eventuelt pensles med vand eller mælk.
8. Eventuelt drysses med knuste hvedekerner.
9. Bages i 12-15 min, hvis det er boller. Bages i ca. 35 min, hvis det er et brød. Ved 225° i begge tilfælde.




## Kilde

Theils Konditori på Farum Bytorv (fra mors samling)

