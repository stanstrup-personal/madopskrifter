---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Hvide boller {.unnumbered}

## Ingredienser

-   150 g margarine
-   ½ L mælk
-   50 g gær
-   1 kg mel
-   1 tsk salt
-   3 tsk sukker

## Procedure

1.  Margarinen smeltes i en gryde.
2.  Mælk og margarine hældes i en skål og blandes.
3.  Gæren røres i.
4.  Resten blandes i.
5.  Hæver til dobbelt størrelse i skålen (½ - 1 time)
6.  Form bollerne
7.  Hæver på pladen ½ - 1 time
8.  Bages i 12 minutter ved 225° C


Portionen giver ca. 30 boller. Boller til frysning pakkes i pose inden de er helt kolde.

Du kan bage franskbrød efter den samme opskrift. Det giver 2 mellemstore brød.  Så er bagetiden 25-30 minutter. Er du i tvivl, om brødet er færdigt, kan du stikke i det med en strikkepind. Der må ikke komme dej med ud.

## Kilde
Mor

