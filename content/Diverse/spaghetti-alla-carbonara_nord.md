---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Spaghetti alla carbonara (nord) {.unnumbered}

![](../_assets/carbonara_cut.jpg)

Til 2 personer.


## Ingredienser

* 225 g spaghetti
* Olie eller smør
* 125 g bacon
* 2 æg
* 2.5 spsk fløde (= 0.4 dl)
* 1 spsk revet parmesan (eller anden grana)
* Salt og peber

## Procedure

1. Kog pastaen i rigeligt kogende og saltet vand. Du skal nok vente lidt med at komme pastaen I til du er længere med bacon.
2. Skær bacon i passende stykker og steg den på en pande ved høj varme. Brug en smule smør/olie til at starte med. Når Baconen er stegt læg det på et stykke køkkenrulle.
3. Pisk æg, fløde, salt og peber i en skål.
4. Når pastaen er færdig fjerner du vandet. Hæld så æggeblandingen over pastaen. Bliv ved med at blande til det er varmet godt igennem og blevet tykkere. Pas på ikke at koagulere æggene, men sæt gryden tilbage på det slukkede blus.
5. Tilsæt bacon og bland.
6. Servér straks og drys eventuelt eksta parmesan og/eller peber ovenpå.


## Kilde
Favoritter Pasta over 100 opskrifter af Ejler Hinge Christensen

ISBN: 978-1-4075-1051-4
