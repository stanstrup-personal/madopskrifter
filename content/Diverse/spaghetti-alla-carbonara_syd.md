---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Spaghetti alla carbonara (syd) {.unnumbered}

![](../_assets/carbonara_syd_cut.jpg)

Til 2 personer.


## Ingredienser

* ½ spsk olie
* 125 g guanciale (pancetta) eller bacon 
* 1 fed hakket hvidløg (kan udelades)
* 2 æg
* 1.2 dL (delt i 2, se procedure) revet Pecorino Romano ost (Grana Padano el.lign. kan bruges)
* 225 g spaghetti
* Salt og peber

## Procedure

1. Kog pastaen i rigeligt kogende og saltet vand (1 spsk. per 2 liter vand).
   Du skal nok vente lidt med at komme pastaen I til du er længere med bacon.
2. Bacon
    * Skær bacon i passende stykker 
    * Kom en smule olie på en pande
    * Tilføj bacon og hvidløg
    * Steg ved medium varme
3. Pisk æg, *halvdelen* af osten og lidt salt i en lille skål
4. Kog pasta
5. Flyt pastaen til den store skål med nogle gafler. Den skal være våd. Smid ikke vandet ud.
6. Tilføj æg/oste-blanding og bland godt og hurtigt.
7. Smag til med salt.
8. Tilføj mere af vandet fra pastaen indtil konsistensen er rigtig.
9. Tilsæt din bacon som det sidste.
10. Servér straks med resten af osten og peber.


## Kilde
https://www.simplyrecipes.com/recipes/spaghetti_alla_carbonara/
