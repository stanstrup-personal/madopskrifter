---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Nonnas kylling og kantareller {.unnumbered}

![](../_assets/nonnas-kylling-kantareller.jpg)

## Ingredienser

-   Kantareller (Nonna bruger "hjemmelavede" tørrede kantareller)
-   Hvidløg
-   Persille
-   Kyllingebryst
-   Mel
-   Fløde
-   Kartofler
-   Rosmarin
-   Salt
-   Peber

## Procedure

-   Start med kartofler. De tager længst. Kartoflerne skæres i mindre stykker (\~1x1 cm) og steges på en pande ved middel varme sammen med rosmarin (brasekartofler). Smag til med salt og eventuelt peber. Læg låg over. Vent til kartoflerne er næsten møre før du tilbereder mere. Lad dem stå ved lav varme hvis de ser ud til at få for meget.
-   Kantarellerne skæres i tynde skiver og steges sammen med hvidløg og persille på en anden pande.
-   Kantarellerne fjernes fra panden og stilles til siden og hvidløg kasseres.
-   Kyllingen skæres i tynde skiver (\~5x5 cm). Eventuelt brug en hammer til at få en ens højde.
-   Kyllingestykkerne paneres let med mel og steges derefter (på samme pande som du brugte til kantarellerne) indtil de har taget farve. Husk salt og eventuelt peber.
-   Nu tilsættes en smule fløde og kantarellerne lægges tilbage på panden. Læg låg over et øjeblik.
-   Når de et stået få minutter under låg og du mener mængden af fløde (kun en smule! De skal ikke druknes!) ser rigtig ud er du klar til at servere.
