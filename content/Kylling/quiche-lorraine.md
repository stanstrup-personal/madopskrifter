---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Quiche Lorraine {.unnumbered}

![](../_assets/quiche1_cut.jpg)
![](../_assets/quiche2_cut.jpg)


## Ingredienser

**Fyld**

* Bacon i tern eller kylling
* Squash eller agurk
* Broccoli
* Champignon
* 4 æg
* 1/2 L mælk
* Salt
* Peber

**Dej**

* Færdig tærtedej eller:
  * 250 g mel
  * 125 g smør
  * 1 æg
  
**Gratinering**

* Emmentaler ost i skiver

## Procedure

* Læg tærtedejen i passende form/fad og varm den i ovnen ved 200 °C til den får en smule farve. Tag den så ud.


**Fyld ** 
* Skær alt fyldet (squash, broccoli, champignon) i mundrette stykker.
* Steg champignon på en pande.
* Brug samme pande til at stege bacontern eller kylling så de er brune.
* Fordel fyldet i tærten (resten bruges rådt).
* Pisk æggene sammen med mælk, og tilsæt salt og peber.
* Hæld mælkeblandingen over.  


* Bag tærten i en forvarmet ovn ved 200 °C i cirka ½ time (ofte mere).


**Gratinering**

* Skær osten i skrimler der er ca 1 cm tykke
* Når tærten har en rimelig fast konsistens lægges osten på overfladen i krydsmønster og bages videre til osten er smeltet og har fået lidt farve (så er tærten færdig).

**Dej (kun hvis du laver din egen. *Aldrig prøvet*)**

* Hæld mel op i et bagefad.
* Skær smørret ud i små tern, og bland dem sammen med melet, til det er homogent og grynet.
* Pisk ægget sammen, og arbejd det ind i dejen, indtil den er jævn.
* Rul dejen ud, og put den i en tærteform.
* Prik huller i bunden med en gaffel.

