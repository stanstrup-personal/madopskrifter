---
created: 2024-12-15T15:29
updated: 2024-12-15T15:48
---
# Kylling i karry med æbler {.unnumbered}

Tilberedningstid ialt: 40 min 
Arbejdstid ialt: 20 min

## Ingredienser
 * 600 g kyllingebryst
 * Olie
 * 25 g smør
 * 2 løg
 * 2 tsk karry
 
 * 1 tsk sukker
 * 1 ½ tsk salt
 * ½ tsk peber
 * ½ dL vand

 * 3 æbler
 * ½ L letmælk 
 
 * 300 g basmati ris

 * Pynt: 4 spsk. frisk persille 
 
## Procedure
 
  * Brun kyllinge-stykkerne på hver side i olie ved hård varme og tag dem op.
  * Smelt smørret i en pande ved middel varme, men uden at brune det.
  * Svits løgene.
  * Tilsæt karry og lad det bruse af.

   * Tilsæt sukker, salt, peber og vand.
   * Læg kødet tilbage i panden.
   * Tilsæt nu æbletern og mælk.
   * Kog det ved svag varme og under låg (vendt kyllingen efter ca. 6 min.) indtil æblerne er blevet bløde.
   
   * Kog risene som anvist på posen.
   
   * Smag til. 
   
   * Pynt med frisk hakket persille lige før serveringen.



## Kilde
Efter https://www.opskrifter.dk/opskrift/494-kylling-i-karry, men timing modificeret.