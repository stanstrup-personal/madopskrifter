---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Jans Tarteletter {.unnumbered}

## Ingredienser

-   \~2 kyllingebryster
-   Smør eller olie
-   Salt
-   Peber
-   1 stort Løg
-   1 flaske asparges (konserves eller friske hvis du kan finde dem)
-   Fond terning\
    \
-   2 store gulerødder
-   Ærter\
    \
-   Smør
-   Mel
-   10 **Store** tarteletter
-   Evt. persille

## Procedure

Retten kan laves ved at man gør tingene klar løbende som de skal bruges. Man har dog travlt, hvis man går det sådan. Husk at have ovnen klar til at varme tarteletterne.

1.  Kyllingen skæres ud i mundrette stykker og steges direkte i den gryde du vil bruge til de er dejligt brune. Brug salt og peber

2.  Tilsæt løg og svits det med.

3.  Asparges skæres ud i 1 cm stykker og tilsættes. Lad dem stege lidt med til noget af vandet er væk.

4.  Tilsæt da nok vand til at det hele er dækket. Tilsæt fond terning.

5.  Det skal nu simre i ca. 20 min

6.  Gulerødder rives på rivejern. De skal tilsættes ca. 5 minutter før de 20 min er gået.

7.  Lav nu en smør/mel-bolle i en anden lille gryde eller lignende.

8.  Hak melbollen i små stykker og tilsæt den til gryderetten. Det skal koge. Tilsæt indtil retten har den rigtige konsistens.

9.  Tilsæt ærter når retten er klar. Varm kun til ærterne lige er blevet varme.

10. Tarteletterne sættes i ovnen når du er ved at være klar. De skal kun have få minutter.

11. Fyld tarteletterne. Sørg for mindst et styk kylling i hver. Du kan give lidt mere fyld med på tallerkenen; der er rigeligt.

12. Pynt eventuelt med persille
