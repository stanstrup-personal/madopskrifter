---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Jans kylling i karry {.unnumbered}

Til 2-3 personer

## Ingredienser

**Kylling**

* ~2 kyllingebryst
* Salt
* Peber

**Ris**

* 250 g basmati ris
* 500 g vand
* Salt

**Sovs**

* Smør
* Mel
* Karry
* Mælk
* Salt

## Procedure

**Kylling**

1. Kyllingen steges i olie eller smør på en varm stegepande. Skal være dejlig brun.
2. brug rigeligt salt og peber.

**Ris**

1. Ris, rigeligt salt og vand varmes på højeste varme i en gryde.
2. Når vandet koger, skrues ned på laveste varme så vandet kun akkurat koger.
3. Risene er færdige når vandet er væk. Sluk da for varmen indtil du er klar.



**Sovs**

1. Karry steges i smør.
2. Mel tilsættes og blandes til en melbolle.
3. En smule mælk tilsættes og piskes grundigt så du har en homogen klump.
4. Mælk tilsættes lidt ad gangen.
5. tilsæt salt.
6. Sovsen er færdig når den koger og har den rigtige konsistens.
