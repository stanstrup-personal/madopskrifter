---
created: 2024-12-15T13:51
updated: 2024-12-22T22:23
---
# Bløde og Karamelliserede Løg {.unnumbered}



## Ingredienser

- 5 stk. store hvide løg (zittauerløg)
- 50 g. smør
- 1 spsk. brun farin
- 1 spsk. balsamicoeddike



## Procedure

1. Pil løggene, del dem på langs og skær dem i tynde skiver.
2. Varm en pande op med smør og steg løgskiverne i 5-10 minutter ved middel varme.
3. Tilsæt brun farin og balsamicoeddike og lad det hele stege 10-15 minutter mere, så løgende begynder at karamellisere.
   Husk at rører rundt i løgende undervejs.
4. Krydr let med salt og tilpas med peber.
5. Tag løgene af varmen, når de har den ønskede farve.


# Kilde
https://vielskermad.dk/opskrift/bloede-loeg/