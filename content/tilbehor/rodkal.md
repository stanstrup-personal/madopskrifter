---
created: 2024-12-15T13:51
updated: 2024-12-16T17:20
---
# Farmors rødkål {.unnumbered}

*Tid:* 1½ time



## Ingredienser

* 1 helt rødkålshoved
* 1 hel flaske sød ribssaft (½ L)
* 3-4 spsk. sukker
* 2 tsk. salt
* 2-3 spsk. eddike



## Procedure

1. *Rødkålshovedet* skæres i tynde strimler,     evt. på råkostapparat. (Det sviner noget.)
2. Rødkålen og *saften* hældes i en stor gryde uden låg.
3. Når det har simret lidt, røres/vendes rødkålen, så den falder     sammen.
4. *Sukkeret* tilsættes.
5. Simrer videre ca. en time. (Pas på ikke at koge det for længe, så     kålen bliver slasket. Den skal være sprød.)
6. *Salt* og *eddike* tilsættes.
7. Rør rundt og smag til, måske skal der lidt mere i af det hele.
8. Gryden tages af blusset og står og hviler lidt, før servering.



Der bliver en stor portion, hvoraf en del kan fryses.

Ved tilberedning af optøet, færdig rødkål, kan man tilsætte lidt ribssaft yderligere.


