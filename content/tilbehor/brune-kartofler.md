---
created: 2024-12-15T13:51
updated: 2024-12-23T15:44
---
# Brune kartofler {.unnumbered}




## Ingredienser

- 800 g kogte og pillede aspargeskartofler
- Ca. 70 g sukker
- 1 dL fløde
- 1-2 dL vand
- 1 tsk. salt
- 1 spsk. smør

## Procedure

1. Varm din pande godt op. 
2. Drys sukkeret på og lad det karamellisere, indtil der dufter af karamel, og sukkeret får en cognac-farve. 
3. Kom smørret på panden og rør den hurtigt rundt. Nu vil sukkeret boble. 
4. Kom fløden på panden lidt ad gangen, ca. 2-3 spsk. pr. gang, og lad det bruse af hver gang du kommer det på, som vist på videoen. 
5. Kom salt i og derefter kom vandet på panden i 4-5 bidder, så du ender med at få en karamellage på panden. 
6. Lad lagen koge igennem i 1 minut. 
7. Tilsæt nu kartoflerne på panden og lad dem suge karamelagen, mens lagen bliver reduceret ved medium varme. 
8. Dine brunede kartofler er klar, når karamellagen er reduceret til en tyk sirup, der kan sidde fast på kartoflerne.


- Du kan lave karamellagen op til en uge i forvejen og sætte den på køl. Du skal blot koge den op og komme kartoflerne i, når du skal lave dine brunede kartofler.
- Hvis du vil lave dine kartofler på forhånd, kan du lave dine brunede kartofler, hvor du ikke reducerer karamellagen helt ned, men lad den være lidt tyndere på panden, så kan du blot give den det sidste, lige inden du skal spise dem.
- Hvis du skal lave brunede kartofler til mange mennesker, kan du lave dem helt færdige på panden og smide dem i ovnen på et fad med låg og holde dem varme ved 110-120 grader.
- Du kan med fordel koge dine kartofler op til 3 dage inden du skal bruge dem.




# Kilde

* [Umut Sakarya](https://www.youtube.com/watch?v=fEszhhwSLGw)

