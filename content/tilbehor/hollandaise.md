---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Hollandaise {.unnumbered}


## Ingredienser

* 150 gram usaltet smeltet smør
* 3 æggeblommer
* 1 spiseskefuld hvidvin
* 1 spiseskefuld citronsaft
* Salt og friskkværnet, hvid peber



## Procedure

1. Smelt smørret og stil den til side.
2. Kom æggeblommer, hvidvin og citron i en skål. Tilsæt også lidt salt og peber.
3. Pisk det luftigt og tykt over et vandbad. Det er her, du bestemmer den endelige konsistens.
4. Tag blandingen *af* vandbadet. Det skal du ikke bruge mere (bruger man vandbadet, mens man tilsætter smør, så bliver det nemt for varm og skiller).
5. Er smøret ikke varmt mere varmer du det lige hurtigt igen.
6. Tilsæt smørret gradvist og forsigtigt. Start med at piske en smule af det smeltede smør ind i æggeblommerne. Tilsæt ikke smørret hurtigere, end du kan få det til at binde i hollandaisen.
7. Smag eventuelt hollandaisen til med lidt mere citronsaft, salt eller peber.
8. servér straks!


## Kilde

Modificeret tilgang efter:

* [madensverden](https://madensverden.dk/hjemmelavet-sauce-hollandaise-den-nemme-opskrift/)
* [jamieoliver](https://www.jamieoliver.com/news-and-features/features/step-by-step-perfect-hollandaise-sauce/)
