---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Burger {.unnumbered}

Denne opskrift er blot til farsen til bøffen.


## Ingredienser

* 500 g hakket oksekød med ~15% fedt
* 1 spsk. Worcestershire sauce (engelsk sovs kan bruges)
* 3 spsk. brødkrummer/rasp (ikke sød)
* 1 fed hakket hvidløg
* 1 lille (eller et halvt) hakket løg
* Salt og peber

## Procedure

1. Bland det hele sammen
2. Form bøffen
3. Drys med salt og peber over bøffen
4. Steg ved hård varme
