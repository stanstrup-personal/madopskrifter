---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Spagetti-kødsovs {.unnumbered}

6-7 personer 

## Ingredienser

* 1 kg hakket oksekød
* 3-4 hakkede løg eller tilsvarende mængde frosset, hakket løg
* 1/2 flaske Beauvais tomatpuré 
* 2 dåser hakkede tomater
* 1 tsk sort peber
* 2 tsk salt
* 2 spsk sød, mild paprika (*Se kommentar under Wikingbøf)
* 1-2 spsk oregano
* Fedtstof til stegning 



**Hertil serveres:** Spagetti (ca. 1/2 til 2/3 pakke med 1 kg alt efter hvem der skal spise det) 
**Tilbehør:** Flittes eller andet madbrød 

## Procedure

1. Start med at sætte en almindelig stor gryde vand til at varme, så den kan blive klar til at koge spagetti i. 
2. Tænd for ovnen, hvis flutene er frosne 
3. Det er vigtigt at bruge en stor gryde med **tyk** bund til kødsovsen 
4. Smelt fedtstoffet i gryden uden det bliver brunt 
5. Hæld først de hakkede løg i og lad det stege nogle minutter 
6. Tilsæt kødet og hak det i mindre klumper med en grydeske i gryden, mens det bruner, så alt kødet bliver gennemstegt. Den røde farve skal være væk, før du fortsætter. 
7. Tilsæt tomatpuré og hakkede tomater 
8. Tilsæt krydderier 
9. Læg låg på gryden og skru ned for varmen 
10. Lad retten koge svagt (simre) under låg i 10 minutter 
11. Nu skulle vandet til spagetti gerne være ved at koge. Tilsæt en lille sjat olie (eller hvad du har af fedtstof) og lidt salt. 
12. Put spagetti i det kogende vand og rod lidt rundt, så intet spagetti ligger klemt, da vandet så ikke trænger ind i spagettien efterhånden som kogningen skrider frem med et dårligt resultat til følge. Af samme grund er det vigtigt, at der ikke er for lidt vand. Spagettien skal koge ca. 10 minutter. Se, hvad der står på pakken. Der kan være lidt forskel.\
Obs! Den friske spagetti, der ligger i kølediskene rundt omkring, skal koge meget kortere tid for ikke at blive ødelagt. 
13. Mens det hele koger de sidste minutter, dækker du bord. 


**Frysning:** Egnet i ~andsportioner, som vi plejer. Bliver tørt i store portioner. Har man brug for at fryse en stor portion samlet, skal man kun fryse selve kødsovsen og evt. tilsætte lidt mere pur ved opvarmningen og koge ny spagetti den dag, retten skal serveres. 
