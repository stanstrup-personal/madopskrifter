---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Rejeretten (thai-rejer) {.unnumbered}

Til 4 personer.

## Ingredienser

-   400 g rejer
-   Smør/Olie
-   Karrypulver
-   3-pak med rød, grøn og gul peberfrugt
-   4 fed hvidløg (oprindeligt 1-2)
-   1 dåse hakkede eller flåede tomater
-   1 dåse tomatpure\
    \
-   1/4 L fløde (oprindeligt 1½ dl)
-   Chilipulver
-   Salt
-   Peber\
    \
-   400 g basmati ris
-   800 g vand
-   Salt

*Hvis du kun er 2 personer kan du med fordel lave halvt så meget ris og bruge sovsen sammen med penne pasta næste dag. Det er super.*

## Procedure

Har du planlagt det i forvejen tager du rejerne ud så de er tøet op. Fjerne væsken og bred dem ud på køkkenrulle så de tørres.

**Sovs**

1.  Skær peberfrugterne i stykker af ca. 1 × 1 cm. Nu er du klar og kan gøre resten løbende.
2.  I en meget varm gryde kommer du olie/smør og rejerne og lader dem svitse.
3.  Hvis rejerne smider meget vand kan du hælde noget af vandet fra. Du behøver ikke fjerne det hele da karry suger en del.
4.  Tilsæt rigeligt karry og lad det svitse med et par minutter. Der skal være fedtstof tilsted så det svitses.
5.  Tilsæt peberfrugterne og de pressede hvidløg. Lad det svitse nogle minutter. *Nu må du gerne starte med risen*
6.  Tilsæt hakket/flået tomat og tomatpure.
7.  Lad det simre. Hvor længe du nu lader det stå afgører hvor smattet peberfrugten bliver.
8.  Når risen er ved at være der tilsætter du fløde (til sovsen!).
9.  Smag til med chili, salt og peber. Der kan også være brug for ekstra karry.

**Ris**

1.  Ris, rigeligt salt og vand varmes på højeste varme i en gryde.
2.  Når vandet koger, skrues ned på laveste varme så vandet kun akkurat koger.
3.  Risene er færdige når vandet er væk. Sluk da for varmen indtil du er klar.
