---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Kokosmælk med karry og Pollock {.unnumbered}

Til 2 (sultne) personer.

## Ingredienser

-   2 stykker Pollock filletter (jeg vil tro man kunne bruge torsk i stedet)
-   1 dåse kokosmælk (eventuelt light)
-   1 spsk. fiskesovs
-   1 spsk. brun farin
-   350 g søde kartofler
-   1 hakket løg
-   1 stor rød peberfrugt
-   9 tsk. gul karry-pasta\
    \
-   Jasmin-ris

## Procedure

1.  Sørg for at fisken er optøet
2.  Steg fisken indtil den falder fra hinanden. Dæk den til og sæt den til side.
3.  Steg karry-pasta med fedtet fra kokosmælken (det faste der ligger øverst). Den oprindelige opskrift siger "indtil fedtet skiller", men jeg synes det er bedre uden det skiller.
4.  Tilsæt resten af kokosmælken, fiskesovs, brun farin, søde kartofler og løg.
5.  Kog indtil kartoflerne er næsten møre.
6.  Tilsæt fisk og peberfrugt.
7.  Kog i 5 min mere og tilsæt ærter.

## Kilde

[WILD ALASKA POLLOCK COCONUT CURRY](https://www.tridentseafoods.com/browse/food-service-recipes/wild-alaska-pollock-coconut-curry/) hos [tridentseafoods.com](tridentseafoods.com)
