---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Paella med fisk {.unnumbered}

![](../_assets/paella1.jpg) ![](../_assets/paella2.jpg) ![](../_assets/paella3.jpg)

Til 4 personer.

Der er uendelige variationer af paella. Her er en der virker super godt.

Der tager længere tid at forberede fisken end man lige skulle tro, så hav god tid.

Alle ingredienserne behøves ikke nødvendigvis, men kan udelades eller udskiftes.

Portionen fylder meget så du skal have en stor wok.

## Ingredienser

**Fisk**

Der kan bruges meget forskelligt fx.:

-   Blæksprutte
-   Torskefillet (jeg brugte 2 stykker. Mere ville også være fint)
-   20 tigerrejer
-   Muslinger (et bundt, husk at mange ikke duer)

**Andet**

-   1 1/4 L fiskebuillon
-   ½ g safron (den oprindelige opskrift siger ½ tsk)
-   Olie
-   1 stort rødløg eller 2 mindre; hakket
-   4 store fed hvidløg eller tilsvarende; presset eller hakket fint
-   2 store røde søde (de lange) peberfrugter, skåret i skiver
-   Små tomater (jeg bruger dem ikke); skåret over
-   400 g ris
-   1 dL hvidvin
-   Ærter
-   (Citron)

## Procedure

**Forbered fisken**

1.  Få fiskebuillon i kog.
2.  Rengør fisk/muslingerne/rejer/blæksprutte grundigt.
3.  Kog fiskene een af gangen i fiskebuillonen. Det er en dødssynd af koge for længe. Kogetiderne er ca:
    -   Torsk: 5 min
    -   Muslinger: 5 min
        -   Husk at muslinger skal være lukkede før du koger dem. Ellers er de døde. De skal åbne når du koger dem. Ellers er de også døde og skal fjernes.
    -   Blæksprutte: 2 min
    -   Tigerrejer: 2 min
4.  Skyld fiskene under kold vand når du tager dem op af buillonen. Stil dem så væk til senere. Gem fiskebuillonen!

**Før risen**

1.  Udblød safran i 2 spsk vand.
2.  Varm olie i en stor wok.
3.  Steg løgene.
4.  Tilsæt hvidløg, peberfrugt, tomater og safran (+ vandet).
5.  Steg 1 minut

**Efter risen**

1.  Tilsæt ris og steg få minutter
2.  Tilsæt hvidvin og det meste af fiskebuillonen, men ikke det hele
3.  Kog 10 min
4.  Smag på risene. De har nok ikke fået nok endnu.
5.  Kog videre indtil risene har den rigtige konsistens (eller næsten) og væsken er væk. Tilsæt eventuelt mere af fiskebuillonen. Smag til med salt og peber.
6.  Når risen er klar så tilsættes fisken og ærterne til risen.
7.  Varm igennem få minutter. Det er ok hvis risene ristes lidt.
8.  Tag wokken fra varmen og lad det hvile 5 minutter.
9.  Servér eventuelt med citron.

## Kilde

2 opskrifter fra nedenstående er slået sammen til en generel opskrift.

Favoritter Fisk og Skaldyr over 100 opskrifter af Ejler Hinge Christensen

ISBN: 978-1-4075-1038-5
