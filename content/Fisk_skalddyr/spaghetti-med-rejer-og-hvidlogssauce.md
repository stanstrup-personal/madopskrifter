---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Spaghetti med rejer og hvidløgssauce {.unnumbered}

Til 4 personer.

## Ingredienser

-   Olie og/eller smør
-   4 (pressede) fed hvidløg
-   2 store Palermo peberfrugt (de aflange røde)
-   1 dåse tomatpure
-   1 dl hvidvin (kan udelades og erstattes af vand)
-   \~ 400 g rejer (mængde ikke så afgørende). Den oprindelige opskrift bruger store rå (grå) rejer. Jeg bruger oftest de almindelige.
-   1/4 L fløde
-   Salt
-   Peber\
    \
-   450 g spaghetti

## Procedure

Du skal bruge en store gryde, da der ermeget pasta. Både færdig pasta og sovsen skal kunne være i gryden.

1.  Peberfrugterne skæres ud i skiver der er ca. 0.8 × 5 cm.
2.  Peberfrugt og hvidløg svitses.
3.  Tilsæt tomatpure og hvidvin.
4.  Kog pasta.
5.  Kom rejerne direkte i sovsen. Tog et par minutter.
6.  Tilsæt fløde.
7.  Smag til med salt og peber.
8.  Hæl det hele over pastaen og bland godt.

## Kilde

Favoritter Pasta over 100 opskrifter af Ejler Hinge Christensen

ISBN: 978-1-4075-1051-4
