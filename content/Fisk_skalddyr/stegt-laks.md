---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Stegt laks med grønne asparges {.unnumbered}

## Ingredienser

-   En lakse-filer per person eller en lakse-side, der skæres ud i filetter. ~150 g per person hvis det er en hovedret.
-   Grønne asparges (ca. et bundt per 2-3 personer)
-   Salt
-   Peber

Der kan serveres [hollandaise sauce](../tilbehor/hollandaise.html) og kartofler til.

## Procedure

Først skal du bestemme dig for om du vil beholde skindet på laksen eller ej. En feinschmecker beholder skindet på. Men da skal du bruge god tid på omhyggeligt at fjerne alle skællene. Jeg fjerner som regel skindet, da jeg synes det er for meget arbejde ellers. Har du ladet skindet blive på og fjernet skællene, så laver du nu nogle tynde snit i skindet uden at skære ind i kødet.

> Åbningen tillader under stegningen fisken at skille sig af med noget af fedtet, der ligger under skindet. Det giver et mere plant og sprødt resultat. \[[euroman](https://www.euroman.dk/gastro/sommerens-fisk--beder-om-at-blive-skindstegt)\]

**Laksen**

1. Tænd for en pande på fuld knald. Kom så olie på. Det må gerne ryge.
2. Laksen krydres med salt og peber.
3. Har du beholdt skindet på, så steger du først på skind-siden. Ellers steg først modsat skind-siden. Ca. 5-7 minutter. Skindet skal bliver sprødt. Er der ikke skind på skal overfladen blive gylden.
4. Smag den opadvendte side til med salt og peber og vend laksen og steg videre i et par minutter.

**Asparges**

1. Vask asparges grundigt.
2. Knæk det nederste stykke af. De knækker selv det rigtige sted (men hold ikke for langt oppe mod hovedet, så knækker den for højt oppe).
3. Tør dem godt.
4. Steg dem på en meget varm pande og smag til med salt og peber.
4. Vendt dem en gang imellem.
5. De skal gerne brankes og blive lidt sorte. Når de har fået farve skruder du ned for varmen.
6. Steg til de har den konsistens, som du gerne vil have.




