---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Indbagte pølser {.unnumbered}


Af denne portion bliver der 24 stk.


## Ingredienser

* 100 g smør
* ½ liter mælk
* 2 tsk. salt 
* 100 g gær 
* 2 æg 
* 200 g grahamsmel
* 800 g hvedemel 

Fyld: 12 grillpølser, der halveres eller 24 brunchpølser 




## Procedure

1. Margarinen smeltes i en gryde ved svag varme. Det må ikke brune.
2. Mælken og salt tilsættes.
3. Den lunkne blanding hældes op i en røreskål.*
4. Gæren røres ud i blandingen.
5. Æggene røres i.
6. Melet æltes godt sammen med resten.
7. Hæver i klump ½ time
8. Dejen formes i 24 flade stykker, der rulles omkring pølserne og lukkes gode**
9. Hæver på pladen i ½ time
10. Ovnen skal være 200 °C, når pladen sættes ind i ovnen.
11. Bages i 15-20 minutter 


\*Blandingen skal være håndvarm, dvs. at den ikke føles hverken varm eller kold, når man mærker med sin rene finger.\
Hvis blandingen er for varm ødelægges gæren og virker derfor ikke. Hvis blandingen er for kold, virker den kun meget langsomt.

\*\* Hvis dejen ikke vil vokse sammen, "limes" den med lidt vand på en finger. Lukningen lægges nedad på pladen.\

Skulle der blive dej tilbage, kan den bages som almindelige boller sammen med de indbagte pølser.

**Frysning:** De brød, der skal fryses, pakkes i pose, mens de endnu er lunkne.
**Optøning:** 2 brød i mikro-ovn, effekt 5-6 (på en skala til 10), 4 minutter. Vendes derefter på brødristeren. 
