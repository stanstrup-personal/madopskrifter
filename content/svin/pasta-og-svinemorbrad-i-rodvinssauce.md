---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Pasta og svinemørbrad i rødvinssauce {.unnumbered}

![](../_assets/pic_pasta_rovvinssovs_cut.jpg)

Til 4 personer.

Der har tit været for lidt sovs. Jeg har prøvet at skrive mit gæt på hvordan der bliver nok. Tilsæt mere vin, hvis der ikke er nok sovs.

## Ingredienser

**Kød**

* 1 svinemørbrad
* Olie/smør
* 225 g champignon
* 1.5 dl fløde
* Salt
* Peber


**Pasta**

* 1 spsk citronsaft (kan udelades)
* 1 knsp safran (kan udelades)
* 350 g orecchioni/conchiglie pasta (billedet er med penne, men conchiglie er bedre)
* Salt


**Æg**

* 12 vagtelæg (har jeg aldrig brugt)


**Sovs**

* Olie/smør
* Hakket løg
* 1 dåse tomat pure
* 4 dl rødvin
* 1 tsk oregano


## Procedure

**Sovs**

1. Svits løget i en kasserolle.
2. Tilsæt tomatpure, rødvin og oregano.
3. Varm og kog lidt ind. Sæt den så til side.

**Kød**

4. Kødet bankes helt tyndt og skæres i strimler (~3x3 cm passer sikkert).
5. Svits kødet på en pande.
6. Tilsæt salt og peber.
7. Tilsæt champignon og svits også dem.
8. Tilsæt sovsen gennem en sigte.
9. Lad det hele simre i 20 min.

**Pasta**

10. Bring rigeligt lettsaltet vand i kog.
11. Tilsæt citronsaft, safran og pasta
12. Kog pastaen færdig

**Færdiggørelse**

13. Fløden tilsættes sovsen/kødet.
14. Varm det hele igennem.

**Æg**

15. Kog vagtelæg i 3 min.
16. Pil ægene
17. skær ægene i halve.

**Servering**

18. Til første runde kan du anrette tallerkenerne:
    * Pasta først
    * Sovs over
    * Pynt med æg
    * Server hurtigt. Bliver hurtigt kold.


## Kilde
Favoritter Pasta over 100 opskrifter af Ejler Hinge Christensen

ISBN: 978-1-4075-1051-4
