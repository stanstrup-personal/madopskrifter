---
created: 2024-12-15T13:51
updated: 2024-12-25T10:43
---
# Mørbradbøffer med bløde løg, svampe og persille {.unnumbered}



## Ingredienser

* 1 svinemørbrad
* champignon
* Smør
* olivenolie
* peber
* salt
* persille

Se ingredienser til [Bløde og Karamelliserede Løg](../tilbehor/blode-og-karamelliserede-log.html).



## Procedure

Se procedure for [Bløde og Karamelliserede Løg](../tilbehor/blode-og-karamelliserede-log.html).

1. Afpuds mørbraden og skær dem i små medaljoner.
2. Gør champignon i stand og rist dem bløde og gyldne i smør på panden, krydr igen let med salt og tilpas med peber.
3. 5. Varm først oliven (1. spsk) op, kom smørret (1. spsk) ved og lad det afbruse.
4. steg mørbradbøfferne, først ved høj varme, på hver side, færdige ved middelvarme, ca. i alt 10 minutter.
5. Tag kødet af panden og anret det på et fad.
6. Skyl, tør og nip persille.
7. Kom løg, champignon og persille tilbage på panden, varm igennem.

## Kilde
https://opskrifter.coop.dk/opskrifter/smaa-moerbradboeffer-med-bloede-loeg-svampe-og-persille-748