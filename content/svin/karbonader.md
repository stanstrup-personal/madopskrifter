---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Karbonader {.unnumbered}

Til 4 personer.



## Ingredienser

* ½ kg hakket flæsk/kalvekød
* 2 spsk. hakket persille
* Ca. ½ tsk. tørret timian
* 1 tsk. salt
* 1 æg
* 3-4 spsk. rasp
* Mel
* Salt, peber
* Fedtstof til stegning



**Tilbehør**

* Kartofler
* Fløde m.m. til sovs




## Procedure

* Ælt kødet sammen med persille, timian, salt, æg og rasp.
* Del kødet i 4 potioner og form hver potion til en bolle.
* Tryk den lidt flad med hånden og hak karbonaderne let på overfladen med en kniv og form dem ensartede.
* Vend karbonaderne i krydret (salt+peber) mel.
* Steg karbonaderne med middel varme i 3 min på hver side, og lad derefter kabonaderne blive gyldenbrune ved svag varme.  


## Kilde
Ditte Henriette Holm på [maduniverset.dk](https://www.maduniverset.dk/opskrift.php/9756/kalv-/-flaesk-opskrifter/Gammeldags-Karbonader)
