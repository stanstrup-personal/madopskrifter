---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Mamas lasagne {.unnumbered}


Til 3 store (stort ildfast fad eller aluminium-fad) lasagne.

**Noter**

Resultatet har været meget varieret så her er nogle specifikke produkter:

* Tomatsouce: Sugo Della Casa [fra Kvickly]
* Mornaysauce: Emmas let mornaysauce [fra Kvickly]
* Lasagneplader: Pastella

## Ingredienser

* 3 gulerødder
* Salt
* 2 store rødløg
* 2 store aubergine
* 4 peberfrugt
* 2 store squash
* 500g oksekød
* 500g svinekød
* 700g tomatsovs
* 1 dåse tomatpuré
* 5 spsk. paprika
* 2 spsk. muskatnød
* 8x 200g friske lasagneplader
* 2 poser grana (revet ost)
* 3x 0.5 L mornay sovs




## Procedure

1. Alle grønsagerne rengøres, hakkes i små stykker og steget i en stor gryde. Tilsæt salt med det samme. Du kan begynde at stege, så snart du er klar med gulerødderne og så tilsætter du resten af grønsagerne, så snart du har dem klar i den rækkefølge, som er angivet ovenfor.
2. Når der ikke er for meget vand tilbage i grønsagerne tilsætter du oksekød og svinekød. Sørg for at det er stegt ordenligt før du går videre.
3. Tilsæt tomatsovs, tomatpuré, paprika og muskatnød.
4. Lad det simre i ca. 1 time.
5. Smag eventuelt til med paprika, salt og muskatnød.
6. Nu bygger du lasagnen (hvert lag skal være *tyndt*. Du skal gerne have plads til 5 lag pasta):
   1. Et tyndt lag mornay-sovs
   2. Lasagneplader
   3. Kødsovs + lidt mornay-sovs + en smule grana (rodes sammen)
   4. gentag 2+3 så mange, som du har plads til.
   5. Sidste lag giver du ekstra grana ovenpå
