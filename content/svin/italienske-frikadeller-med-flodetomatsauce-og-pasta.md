---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Italienske frikadeller med flødetomatsauce og pasta {.unnumbered}
Til ~4 personer.

Dejlig let ret. Frikadellerne har meget smag. Sovsen kunne godt være mere spændende.

![](../_assets/frikadeller.jpg)

## Ingredienser

* Farfalle pasta (dem der ligner en sommerfugl)
* Salt


**Fars**

* 500 g hakket svinekød eller kalv/svinekød.
* 1 æg
* 1 dl mel
* 1 dl mælk
* 3 fed hvidløg
* 3 spsk oregano (frisk eller tørret)
* 3 spsk basilikum (frisk eller tørret)
* 1 tsk køkkensalt
* 1/2 tsk peber
* 150 g mozzarella
 
 
**Sauce**

* 1 dåse flåede eller hakkede tomater
* 1/4 L fløde
* 2 fed hvidløg
* ½ tsk bordsalt
* ½ tsk peber


## Procedure

1. Start med sovsen. Den har godt af at stå længe. 
2. Alle ingredienser til sovsen blandes og den skal så bare simre. Smag til når du er ved at være klar. Eventuelt kan du tilsætte tomatpure.


3. Bland alle ingredienserne til farsen sammen.
4. Frikadellerne formes og steges på en pande.
5. Hold dem eventuelt varme i ovnen. Med en normal stor pande skal du bruge 3 runder for at få dem alle lavet.


6. Kog pastaen


## Kilde
http://opskrifter.dk/Opskrift_100346.100.10.html

