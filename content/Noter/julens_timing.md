---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Julemadens timing {.unnumbered}

**Opskrifter som bruges i denne guide:**

* [Langtidsstegt hel and](../Vildt/langtidsstegt-hel-and.html): med blåt er markeret det der har med anden at gøre
* [Rødkål](../tilbehor/rodkal.html)
* [Brune kartofler](../tilbehor/brune-kartofler.html)
* [Risalamande](../Dessert/risalamande.html)

<br><br>

## 23. Dagen før

| Start tid             | Hvad                                                         | Noter                                                        |
| --------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Formiddag/middag      | Kog risengrød til risalamande                                | Skal nå at køle ned før aften. Det kan tage længe            |
| Formiddag/Eftermiddag | [Rødkålen](../tilbehor/rodkal.html) skæres, lægges i lukket pose og i køleskab | Husk at gemme saften også!                                   |
| Formiddag/Eftermiddag | Eventuelt lav [rødkålen](../tilbehor/rodkal.html) færdig     | Så for at den er kogt lidt for lidt så den er rigtig når den opvarmes den 24. hvis du laver den nu. |
| Aften                 | Eventuelt dæk bord                                           |                                                              |

<br><br>

## 24. Juleaftensdag

| Start tid | Hvad                                                         | Noter |
| --------- | ------------------------------------------------------------ | ----- |
| 9:30      | <span style="color: blue">Start på at gøre anden klar</span> |       |
| 9:50      | Brun rester fra anden til sovsen                             |       |
| 10:00     | Klargør grønsager osv. til sovsen                            |       |
| 10:40     | <span style="color: blue">And ind i ovnen</span>             |       |
| 10:50     | "Brune" kartofler koges, pilles og sættes væk til senere     |       |
| 11:00     | Risalamande laves færdig                                     |       |
| 12:00     | En smule frokost spises for at holde pendulet i sving        |       |
| 13:00     | Rødkål laves færdig                                          |       |
| 15:00     | Sigt indholdet fra bradepanden over i en gryde               |       |
| 15:10     | <span style="color: blue">Temperatur op på ovnen</span>      |       |
| 15:50     | "Hvide" kartofler koges                                      |       |
| 16:00     | Sovsen færdiggøres                                           |       |
| 16:10     | <span style="color: blue">And ud</span>                      |       |
| 16:20     | "Hvide" kartofler pilles og sættes i seng                    |       |
| 16:30     | "Brune" kartofler brunes                                     |       |
| 16:40     | <span style="color: blue">Partér anden</span>                |       |
| 16:50     | <span style="color: blue">Eventuelt lidt mere grill i ovnen til at varme anden</span> |       |
| 17:00     | <span style="color: blue">Start på middagen</span>           |       |

