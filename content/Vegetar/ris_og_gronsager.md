---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Ris med grønsager {.unnumbered}

![](../_assets/ris-med-gr%C3%B8nsager.jpg)

## Ingredienser (3-4 personer)

-   250 g basmati ris
-   450 g vand
-   salt
-   Olivenolie\
    \
-   1 lille squash
-   1 lille gulerod
-   \~ 70 g ærter
-   1gul og 1 rød peberfrugt
-   1 løg\
    \
-   2-3 skefulde soja-sovs (tilsæt efter smag)

## Procedure

### Ris

1.  Kog risene i 10 min eller indtil vandet er væk i en gryde med låg:
    -   \+ vand, salt og lidt olivenolie
2.  Lad risen hvile i 10 min
3.  Med en gaffel brydes klumper op
4.  Dæk risen til med plastik og sæt den i køleskabet

### Grønsager

5.  Vask og skær squash (det midterste vandige fjernes), gulerod og peberfrugt i Julienne (tyndeskiver)
6.  Snit et løg i små stykker og steg det blødt med lidt olie og en lille smule vand
7.  Tilsæt ærter og skru op for varmen
8.  Når løg og ærter er bløde tilsæt resten af grønsagerne
9.  Tilsæt soja og ris
10. Tilsæt mere soja efter smag, bland hurtigt og varm i 1-2 minutter. Bland igen og varm et minut mere.
11. tilsæt mere soja til smagen er rigtig.

Kan også serveres koldt.

## Kilde

[Una Riccia ai Fornelli](https://blog.giallozafferano.it/ricciaifornelli/riso-basmati-con-verdure-ricetta-thailandese/)
