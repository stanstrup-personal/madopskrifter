---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Fusilli med gorgonzola-svampesauce {.unnumbered}
Til 2 personer.
Mængderne er som jeg husker det. Har ikke lavet den siden jeg skrev den ned.

## Ingredienser

* 230 g fusilli pasta\
  \
* Olie og/eller smør
* 125 g svampe. Jeg foretrækker portobello svampe. Ellers brune eller de almindelige champignon.
* 1 fed pressede hvidløg
* 1/4 L fløde
* 75 g gorgonzola
* Salt
* Peber

## Procedure

1. Steg svampene i en pande
2. Tilsæt hvidløg
3. Tilsæt fløde
4. Tilsæt gorgonzola og sørg for at den "opløses"
5. Kog pasta (husk salt)
6. Smag sovsen til med salt og peber
7. Vend pastaen i sovsen.

## Kilde
Favoritter Pasta over 100 opskrifter af Ejler Hinge Christensen

ISBN: 978-1-4075-1051-4


