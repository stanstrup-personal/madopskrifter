---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Græskarsuppe {.unnumbered}


## Ingredienser
* 1 løg
* 1 lille græskar
* 2 gulerødder
* 2 kartofler
* 1 grønsags-bouillonterning
* 1 tsk. muskatnød
* Vand
* 1 spsk. olienolie
* Grana (revet ost)

## Procedure

1. Snit løget og svits det i en gryde
2. Skær alle grønsagerne i tern og smid dem i gryden
3. Tilsæt grønsags-bouillonterning og muskatnød
4. Tilsæt nok vand til at dække det hele
5. Kog til grønsagerne er bløde
6. Blend det hele
7. Før servering tilsættes olie og grana

## Kilde
Nonna


