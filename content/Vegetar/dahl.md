---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Dahl med røde linser {.unnumbered}



## Ingredienser

* 300 g tørre røde linser
* 1 stor (~200 g) gulerod i fine tern
* 1 lille peberfrugt
* 1 stort løg, hakket
* 4 fed hvidløg, hakket
* 1 spsk. (med top) frisk hakket ingefær
* ½ sps.k vegetabilsk olie
* 3 kopper (720 ml) grøntsagsbouillon eller vand
* 240 mL kokosmælk på dåse
* 1 ½ tsk. stødt spidskommen
* 1 spsk. karrypulver
* 1/2 spsk. valgfrit sødemiddel
* 1 tsk. stødt gurkemeje
* 1 tsk. paprika
* Salt og sort peber efter smag
* 1/3 tsk. rød chilipeberflager (valgfrit)

* basmatiris
* Evt. kartofler eller naan





## Procedure

1. Skyl linserne under rindende vand. Hak løg, hvidløg, ingefær, peberfrugt og gulerod.
2. Varm olie op i en gryde og svits løgene i cirka 3-4 minutter ved middel varme.
3. Tilsæt ingefær, hvidløg, gulerod og peberfrugt.
4. Tilsæt alle krydderierne, sødemiddel, linser og grøntsagsbouillon eller vand.
5. Bring i kog og lad simre i cirka 10 minutter.
6. Til sidst tilsættes kokosmælk og det hele koges i yderligere 5 minutter eller indtil den ønskede tykkelse af dahlen er nået.
7. Smag til med sort peber og salt.
8. Smag til og juster krydderierne efter behov.


Serveres lun med basmatiris, kartofler eller naan (fladbrød) og pynt med friske krydderurter.

Kan også serveres koldt.

## Kilde

[elavegan.com](https://elavegan.com/red-lentil-dahl/)
