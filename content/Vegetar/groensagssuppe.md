---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Grønsagssuppe (Minestrone) {.unnumbered}


## Ingredienser
* 1 løg
* 2-3 kartofler
* 3 gulerødder
* 1 bladselleri
* 1 stor squash
* 2-3 tsk. tomatpuré
* 1 håndful Borlotti bønner (de skal i vand 1 dag før!)
* 1 grønsags-bouillonterning
* Vand
* 1 spsk. olienolie
* Grana (revet ost)
* Evt. en lille pasta-type

## Procedure

1. Snit løget og svits det i en gryde
2. Skær alle grønsagerne i tern og smid dem i gryden
3. Tilsæt tomatpure, bønner og  grønsags-bouillonternin
4. Tilsæt nok vand til at dække det hele
5. Kog i 20-30 min i trykkoger
6. Blend det hele
7. Tilsæt pasta
8. Før servering tilsættes olie og grana

## Kilde
Nonna


