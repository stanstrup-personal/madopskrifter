---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Kærnemælkskoldskål {.unnumbered}




## Ingredienser

* 1 æg (evt. pasteuriseret)
* 2 spsk. sukker 1 tsk.
* vanillesukker
* 1/2 L kærnemælk 


## Procedure

1. Æg, sukker og vanillesukker blendes.
2. Der tilsættes 1/2 liter kærnemælk og blendes igen.

Husk, at koldskålen ikke må blive stående i blenderen, da syren så fjerner metalioner fra blenderens kniv og det kan smages. 

