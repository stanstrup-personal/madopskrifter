---
created: 2022-08-09T17:21
updated: 2024-12-15T13:51
---
# Oles kanelæblekage {.unnumbered}

## Ingredienser (meget stor portion!)

-   500 g smør
-   500 g sukker + lidt mere
-   8 æg
-   600 g hvedemel
-   4 tsk bagepulver
-   2 dL mælk
-   8 æbler
-   kanel

## Procedure

1.  Rør det smeltede smør og sukker sammen.
2.  Tilsæt æggene, et ad gangen.
3.  Bland mel, bagepulver og mælk i.
4.  Skær de skrællede æbler i terninger.
5.  Læg halvdelen af dejen i formen.
6.  Tilsæt æble-stykkerne og drys med kanel. Drys eventuelt med sukker hvis æblerne er sure.
7.  Tilsæt dernæst resten af dejen.\
    Formen fyldes kun 2/3 op!
8.  Bag kagen i 40 min ved 180 °C.

## Kilde

Onkel Ole fra skolen og modificeret af mor.
