---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Havregrynskugler {.unnumbered}


## Ingredienser

* 125 g smør
* 250 g havregryn
* 50 g kakao
* 2 spsk. kaffe (flydende)
* 2 spsk. fløde eller mælk
* 2 tsk. mandelessens
* 2 tsk. romessens
* 200 g sukker


## Procedure

1. Ælt det hele godt sammen
2. Tril til små kugler

Opbevares i køleskab

