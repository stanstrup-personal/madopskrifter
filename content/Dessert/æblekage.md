---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Æblekage {.unnumbered}

## Ingredienser

-   1 pose **makroner**
-   1 pose **æblekagerasp**
-   1 flaske **sherry** (Amontillado skal der stå, hvis du ikke kan få Fad No 6.)
-   Lidt mere end 1 liter **æblemos** (æblegrød) pr. æblekage
-   Lidt mere end 1/4 liter **piskefløde** pr. æblekage\

Der er nok æblekagerasp og makroner til 2 kager.

## Procedure

**Obs!** Æblekage lægges sammen **senest 3 timer før**, den skal bruges!\
Den kan fint laves dagen før. Evt. først flødeskum samme dag.\
\

1.  Læg makronerne tæt i bunden af skålen (bedst er en skål med bred bund). Knus 4-5 stykker til sidst til at udfylde hullerne imellem de andre.
2.  Dryp Sherry på så det sejler lidt. (Makronerne tager nogen tid om at suge det op, så der skal være lidt for meget lige nu.)
3.  Derefter et lag æblemos 4-6 cm tykt, så et lag rasp ½-1 cm tykt. (2 tyndere lag af begge dele, hvis æblemosen blev lidt tynd, så suger raspen noget af vandet.)
4.  Flødeskum til sidst. (Flødeskummet må ikke kommes på en varm æblekage, da det så klasker sammen.)

## Kilde

Mor
