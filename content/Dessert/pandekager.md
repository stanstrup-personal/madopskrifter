---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Pandekager {.unnumbered}

![](../_assets/pandekager1.jpg)
![](../_assets/pandekager2.jpg)

Ca. 8 pandekager

## Ingredienser

* 80 g hvedemel (ca. 1 ¼ dL)
* 1 spsk sukker
* 1 tsk groft salt
* Revet skal af en halv citron
* 3 dl mælk
* 3 æg


## Procedure

1. Bland mel, sukker, salt og citronskal
2. Pisk mælken i lidt ad gangen til en klumfri dej.
3. Pisk æggene i et ad gangen
4. ~ 1 dl per pandekage på en medium-størrelse pande. Du kan bruge mindre hvis du vil have dem helt tynde.
5. Der skal hele tiden være smør på panden. Ellers brænder det på.
6. Hæld det hele hurtigt på midten og fordel.
7. Servér med is og/eller frugt.


## Kilde
Mors modificerede version.
