---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Krydderkage {.unnumbered}

## Ingredienser

-   2 æg
-   3 dL sukker\
    \
-   4 dL hvedemel
-   2 tsk. bagepulver
-   2 tsk. kardemomme
-   2 tsk. kanel\
    \
-   1½ dL mælk\
    \
-   100 g smør

## Procedure

1.  Tænd ovnen på 200° C, inden du begynder, så den er varm, når du er klar.
2.  Æg og sukker piskes til æggesnaps.
3.  Mel, bagepulver, kardemomme og kanel blandes.
4.  Melblandingen hældes i æggesnapsen og røres godt.
5.  Mælken tilsættes lidt ad gangen.
6.  Smørret smeltes og tilsættes lidt ad gangen.
7.  Bages ved 200° C i 30 til 40 minutter. Tag hensyn til kagens tykkelse.

For at vide, om bagepulverkage er bagt færdig, tager man en strikkepind eller lignende og stikker ind i kagen. Der må ikke hænge noget (dej) fast på strikkepinden, når man tager den ud. Gør der det, skal du bage videre og prøve igen om 5 minutter. Der er ikke så stor tildsforskel på færdig og næsten færdig, hvis kagen ellers ser færdig ud i skorpen.

**Frysning:** Kagen kan bages i dobbelt portion of fryses. Den skal være helt kold, før man pakker den til frysning.

\## Kilde

Mor
