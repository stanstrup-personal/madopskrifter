---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Drømmekage {.unnumbered}

## Ingredienser

**Kage**

* 4 æg
* 300 g sukker
* 250 g hvedemel
* 3 tsk bagepulver
* 1 tsk vaniljesukker (jeg bruger gerne mere)
* 2 dl mælk
* 50 g smør


**Fyld**

*Her har jeg sørget for at der er rigeligt*

* 150 g smør
* 225 g kokosmel
* 340 g brun farin
* 3/4 dl mælk


## Procedure

1. Pisk æg og sukker grundigt.
2. Tilsæt mel, bagepulver og vaniljesukker. (i den oprindelige opskrift står der at man gør det separat. Har jeg aldrig gjort og det går fint.)
3. Bland/Vend det hele forsigtigt.
4. Varm mælk og smør (lunkent) og sæt det til dejen lidt ad gangen.
5. Hæl dejen i en passende bradepande eller lignende.
6. Bag ved 200 °C i 25 min (oprindeligt 20 min, men så er den ikke færdig i midten. Den skal være ret mørkebrun før den er færdig.)
7. Imens kagen bages laver du fyldet.
8. Kom alle ingredienser i en gryde og bring det forsigtigt i kog.
9. Når kagen har bagt de 20 min tager du den ud og fordeler fyldet ovenpå. Det har det med at glide nedad så du kan med fordel komme lidt mere på i midten.
10. Kagen tilbage i ovnen og bag færdig i 5 min ved Bag ved **225** °C.


## Kilde

https://www.arla.dk/opskrifter/drommekage-fra-brovst/
