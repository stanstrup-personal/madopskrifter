---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Æblegrød {.unnumbered}

## Ingredienser

-   30 æbler
-   2-3 dL vand
-   1 dL sukker

## Procedure

1.  Æblerne skrælles, kernehuset fjernes og æblerne skæres i både.
2.  Disse koges under låg i vand til de er møre og koger ud, når man rører rundt. \
    Der skal forblive små klumper æble i. Jo mere modne æblerne er, jo mindre vand skal der i. (Ikke for meget fut under, så koger det over.)\
    Til en gryde fuld, 30 æbler, startes med 2-3 dl vand.
3.  Kik til det tit, rør lidt rundt så de øverste æbler kommer nedad, og se, om der skal mere vand i.
4.  Først når det er mos tilsættes sukker. Også dette er svært at sige hvor meget, da det afhænger af æblernes modenhed og sødme. Start med 1 dl sukker til 30 æbler og smag på det.


Til æblekage må mosen ikke være for sød, da rasp og makroner også gør det sødt. Den skal være sådan, at hvis du skulle spise det som grød, ville du drysse sukker på. Æblegrøden behøver ikke være helt kold, før du lægger æblekagen sammen. Det er klogest at lave i hvert tilfælde grøden dagen før (eller tidligere og fryse den), da du bliver forbavset over, hvor lang tid det tager både at lave den og få den ned i temperatur.



## Kilde

Mor
