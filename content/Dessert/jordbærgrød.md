---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Jordbærgrød {.unnumbered}

## Ingredienser

-   1 kg jordbær
-   Ca. 2 dl vand (= 1 almindeligt glas fuldt)
-   4-5 spsk. Sukker
-   Lidt kartoffelmel til jævning

**Hertil serveres:**

-   Mælk
-   Evt. sukker

## Procedure

1.  Jordbær og vand koges
2.  Sukkeret tilsættes og der koges et øjeblik videre til sukkeret er opløst
3.  Gryden tages af blusset. Grøden må ikke koge, når jævningen hældes i og heller ikke igen bagefter.
4.  En jævning af 2 spsk. kartoffelmel med lidt vand tilsættes langsomt. Måske skal det hele ikke i. Grøden bliver meget tykkere, når den bliver kold. I varm tilstand er en konsistens som yoghurt passende.
5.  Brug en skål, der ikke er for sart og som kan tåle varme-kulde-shock og hæld grøden i straks, så den ikke bliver tyk i gryden. Det går ret hurtigt.

\

-   Man skal holde lidt øje med kogningen, da det går ganske stærkt og nemt koger over.
-   Jordbærgrød og anden frugtgrød, der er jævnet med kartoffelmel kan ikke fryses.
-   Æblegrød bliver ikke jævnet med noget som helst, så derfor går det godt at fryse æblegrød.

## Kilde

Mor
