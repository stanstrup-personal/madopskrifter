---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Rabarberkokostærte {.unnumbered}

![](../_assets/Rabarbertaerte_pic_cut.jpg)

_Billedet viser en dobbelt-portion._

Opskriften siger til 4 personer, men så er de meget sultne!

## Ingredienser

* 350 g rabarber
* 125 g sukker (til rabarberen, se opskrift)
* 3 tsk vanilla \(oprindelige 1 tsk\)\
  \
* 125 g smør\
  \
* 125 g sukker (til dejen, se opskrift)
* 1 æg
* 80 g hvedemel \(oprindeligt 60 g\)
* 2 tsk bagepulver
* 100 g kokosmel\
  \
* Creme fraiche

## Procedure

1. Rabarberne snittes. Ca. ½ cm tykke.
2. Rabarber, sukker og vanilla blandes. Hvis Rabarberne er frosne lad det gerne stå noget tid.
3. Adskilt fra rabarber tager du smøret og varmer det \(lunt\) i en gryde og bland så med sukker og æg. Pisk godt.
4. Tilsæt mel, bagepulver og kokosmel til ovenstående.
5. Halvdelen af dejen \(eller lidt mindre\) smøres nu ud i et ildfast fast eller lignende.
6. Læg rabarberne på.
7. Fordel resten af dejen. Det kommer til at se ud som om der er for lidt dej. Det skal nok gå.
8. Bag ved 185 °C i 45 min. Pas på den ikke får for lidt og bliver til kompot.
9. Servér med creme fraiche.



