---
created: 2024-12-15T11:27
updated: 2024-12-15T13:51
---
# Risengrød {.unnumbered}

Tilberedningstid: 45min. 
Antal: Til 4 personer.

## Ingredienser
* 2 1/4 dL grødris (ca. 190 g)
* 2 ½ dl vand
* 1 liter mælk (helst sød- eller letmælk) 
* ½ tsk salt


Drys risene i kogende vand og lad dem koge i ca. 2 min. Tilsæt maelken og bring blandingen i kog under omrøring. Kog grøden ved svag varme og under låg - rør jævnligt. Tilsæt salt og smag til. 

Server grøden med kanelsukker, smørklat, saftevand eller hvidtøl. 

**Tip:** Har du en rest risengrød tilovers, kan du lave en omgang klatkager til dessert en anden dag. Rør risengrøden med kartoffelmel, citronskal, sukker, vaniljekorn og æg til en dej, der bages i smør på en varm pande

## Kilde

Mor
