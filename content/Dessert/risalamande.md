---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Risalamande {.unnumbered}

4-6 personer

## Ingredienser

* 1 sødmælk
* 125 g grødris
* ½ tsk salt
* 2-3 spsk sukker
* 1-3 tsk vanillesukker
* 2½ dL piskefløde
* 50 g smuttede, hakkede mandler (husk at tage een fra!)


## Procedure

1. Bring mælken i kog (gerne i gryde smurt med smør)
2. Tilsæt ris under omrøring
3. Dæmp varmen så det koger sagte under låg
4. Lad det koge ca. 1 time indtil konsistensen er rigtig. Det ser altid mere flydende ud når det er varmt, så der må godt være noget væske.
5. Tilsæt salt og sukker
6. Rør nogle gange under afkøling



*Når grøden er kold*

6. Tilsæt hakkede mandler
8. Vendt forsigtigt flødeskum ind i grøden
8. Smag til med vanillesukker

