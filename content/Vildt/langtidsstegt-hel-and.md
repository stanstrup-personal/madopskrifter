---
created: 2024-12-15T13:51
updated: 2024-12-16T17:16
---
# Langtidsstegt hel and {.unnumbered}


Husk at stegetiden totalt er 5½ time + den skal hvile mindst 30 min!




## Ingredienser

- 1 stk. and af god kvalitet (ca. 3 kg) – er den frossen skal den tages op 2-3 dage før tilberedning.
- Salt



**Fyld**

- 1 appelsin
- 2-3 æbler gerne syrligt
- 2-3 rødløg
- 300 g svesker
- 2 spsk. tørret timian (eller ~5 kviste frisk)
- 3 laurbærblade



**Andesovs**

- 2 store gulerødder
- 3 stængler blegselleri (eller bladselleri)
- 1 stort løg
- 2 fed hvidløg
- Saft fra 1 appelsin
- 5 laurbærblade\
  \
- Salt/peber
- Olie
- 1-2 dl rødvin\
  \
- 1 liter vand
- Ribsgele
- Eventuelt citron
- Eventuelt fløde





## Procedure

**Rengør**

1. Fjern og gem indmaden.
2. Tjek efter at anden er ordenligt soigneret og skær flommefedt fra og klip vingespidserne af (gem dem).
3. Skyl *anden* inden i - indtil vandet ikke tager farve af blod. 



**Fyld**

1. Skær *appelsinen* i grove tern.
   Fjern kernehuset fra *æblerne* og skær det i både.
   Steg *løgene* i smør eller andefedt et par minutter.
   Tilsæt så *æblerne* og *sveskerne* og *appelsin-stykker*. Giv det hele ca. 5-6 minutter. Slut af med at vende *laurbær* og timian ved.
2. Gnid anden godt ind i salt på både inder- og yderside (jeg brugte et par spiskefuld). Fyld anden op med dit fyld og luk den tæt med et par kødnåle



**Sovsen (bradepande sammen med anden!)**

1. Rengør alle *grøntsagerne* og skær dem ud i mindre stykker

2. Læg *grøntsagerne*, *laurbærblade* og *appelsinsaft* i en bradepande sammen med *indmaden* fra anden

3. Giv den en grundig omgang salt og peber og hæld et par fuldvoksne skvæt olie eller andefedt plus rødvin over

4. Sæt bradepanden i ovnen under anden

5. Efter et par timer tid hælder du vand ved og lader det være

   

6. Når din and er færdig tager du også bradepanden ud og hælder alt indholdet fra bradepanden ned i en si over en gryde

7. Sørg for at skrabe alt af bradepanden og mas det godt igennem sien

8. Nu koger du massen godt ind til den har en passende konsistens, det tager ca. 30 minutter, samtidig med, at du skummer fedt og urenheder fra undervejs.

9. Herefter smager du til med salt og peber 

10. justér evt. med lidt citron og ribsgele

11. Evt. tilsæt fløde



**Varm**

1. Varm din ovn op til 200 °C (gammeldags ovn, ikke varmluft!) og sæt bradepanden/fadet i ovnen sammen med din fyldte and. Jeg sætter anden på en rist henover bradepanden, så fedt og saft kan smelte fra og falde ned i bradepanden. **Skru ned til 110 grader med det samme**, og lad anden stege i 4,5 time – husk at hælde lidt væde fra bradepanden henover anden ca. hvert 30 minut.
2. Efter de 4,5 time tager du det meste af væden fra bradepanden og hælder det i en stor skål, så det kan køle lidt ned, og fedtet derved kan skilles fra fonden (gem fedtet i en beholder for sig, det er fantastisk at stege ting i, og det kan holde sig længe på køl). Skru ovnen op på 180 °C og lad anden stege en times tid, eller til skindet er sprødt – husk stadig at hælde lidt væde på anden cirka hver halve time. 
3. Hvis skindet ikke er sprødt til sidst, skruer du op på 225 °C, sætter ovnen på grill og holder skarpt øje med anden – tag den ud med det samme skindet er klar (det tager typisk 5-10 minutter).



**Hvil og klargør**

1. Lad anden hvile i mindst 30 minutter, gerne 45-60, før du parterer og serverer den.
2. Når anden har hvilet færdigt tager du svesker, æbler og appelsin ud – kassér appelsinen og server æble/svesker sammen med anden, som du parterer og skærer i passende stykker.



## Kilde

- [madsvin.com](https://madsvin.com/langtidsstegt-and-den-bedste-andesteg)

- [gastromand.dk](https://gastromand.dk/opskrift-langtidsstegt-helstegt-and)

- [valdemarsro.dk](https://www.valdemarsro.dk/andesteg)

  
