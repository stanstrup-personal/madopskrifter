---
created: 2024-12-15T13:51
updated: 2024-12-15T13:51
---
# Langtidsstegt andebryst-steg på skrog {.unnumbered}



![](../_assets/andesteg_cut.jpg)

Til ~ 4 personer (ikke grovæddere) afhængigt af størrelsen.


## Ingredienser

* 1 stk andebryst-steg på skrog
* Salt
* Peber
* God tid!

## Procedure

1. Kom anden i et ildfast fad eller lignende.
2. Gnid med salt og peber.
3. Steg ved 100 °C i **4-5 timer** (almindelig ovn; ikke varmluft!).
4. Når stegen begynder at smide væske tager du engang imellem steget ud og hælder væsken over skindet. Du kan løbende tage stegesky (og fedt) fra til sovsen, så anden ikke kommer til at koge i fedt.
5. Skru til sidst op på 200 °C i ca. 20 min.
6. Giv til sidst grill til skindet er sprødt.
7. Lad steget hvile ca. 15 min.
8. Skær stegen ud.

## Kilde

http://bedste-opskrifter.dk/#!/opskrift/305/langtidsstegt_andebryst-steg
